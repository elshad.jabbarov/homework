package Homework5;

import java.util.ArrayList;
import java.util.Objects;

public class Family {
    Human Mother;
    Human Father;
    ArrayList<Human> children;
    int index = 0;
    Pet pet;

    protected Family(Human mother, Human father) {
        Mother = mother;
        Father = father;
        this.children = new ArrayList<>();
    }


    protected void addChild(Human child) {
        children.add(child);
    }

    protected void deleteChild(Human child) {
        children.remove(child);
    }

    protected int countFamily(){
        return 2+children.size();
    }



    public Human getMother() {
        return Mother;
    }

    public void setMother(Human mother) {
        Mother = mother;
    }

    public Human getFather() {
        return Father;
    }

    public void setFather(Human father) {
        Father = father;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }



    @Override
    public String toString() {
        return
                "Mother=" + Mother +"\n"+
                " Father=" + Father +"\n"+
                " children=" + children +"\n"+
                " pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(Mother, family.Mother) &&
                Objects.equals(Father, family.Father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Mother, Father, children, pet);
    }
}
