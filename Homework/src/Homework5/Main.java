package Homework5;

public class Main {
    public static void main(String[] args) {

        Human Father;
        Father = new Human("Albert", "Einstein", 1879, 15);
        Human Mother;
        Mother = new Human("Elsa", "Einstein", 1869, 16);
        Pet dog = new Pet("dog", "rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human Eduard;
        Eduard = new Human("Eduard", "Einstein", 1960, 100);
        Human Frank;
        Frank = new Human("Frank", "Einstein", 1970, 600);
        Family Einsteins = new Family(Father, Mother);

        Einsteins.addChild(Eduard);
        Einsteins.addChild(Frank);
        System.out.println(Einsteins.toString());
        Einsteins.deleteChild(Eduard);
        System.out.println(Einsteins.toString());
        System.out.println(" The Number of Family member is  "+Einsteins.countFamily());
    }
}
