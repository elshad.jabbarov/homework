package Homework5;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private String species;
    private String nickname;
    private Integer age;
    private Integer trickLevel;
    private String[] habits;

    Pet(String species, String nickname, Integer age, Integer trickLevel, String[] habits) {
        this.setSpecies(species);
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
    }

    private Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    private Pet() {
    }

    private void eat() {
        System.out.println("I am eating");
    }

    private void respond() {
        System.out.println("Hello, owner. I am " + getNickname() + " I miss you!'");
    }

    private void foul() {
        System.out.println("I need to cover it up");
    }

    private String getSpecies() {
        return species;
    }

    private void setSpecies(String species) {
        this.species = species;
    }

    private String getNickname() {
        return nickname;
    }

    private void setNickname(String nickname) {
        this.nickname = nickname;
    }

    private Integer getAge() {
        return age;
    }

    private void setAge(Integer age) {
        this.age = age;
    }

    private Integer getTrickLevel() {
        return trickLevel;
    }

    private void setTrickLevel(Integer trickLevel) {
        this.trickLevel = trickLevel;
    }

    private String[] getHabits() {
        return habits;
    }

    private void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return species + "{" +
                " nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return species.equals(pet.species) &&
                nickname.equals(pet.nickname) &&
                age.equals(pet.age) &&
                trickLevel.equals(pet.trickLevel) &&
                Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }
}

