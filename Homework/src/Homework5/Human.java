package Homework5;

import java.util.Objects;

public class Human {
    String name;
    String surname;
    Integer year;
    Integer iq;

    Human(String name, String surname, Integer year, Integer iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    private String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    private String getSurname() {
        return surname;
    }

    private void setSurname(String surname) {
        this.surname = surname;
    }

    private Integer getYear() {
        return year;
    }

    private void setYear(Integer year) {
        this.year = year;
    }

    private Integer getIq() {
        return iq;
    }

    private void setIq(Integer iq) {
        this.iq = iq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(year, human.year) &&
                Objects.equals(iq, human.iq);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }

    @Override
    public String toString() {
        return  "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                '}';
    }
}
