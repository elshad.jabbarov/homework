package Homework6;

public interface Interface {
    boolean canFly();

    boolean canSwim();

    boolean hasFur();

    int numberOfLegs();
}
