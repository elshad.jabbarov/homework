package Homework6;

public enum Species implements Interface {
    Invertebrates {
        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public boolean canFly() {
            return true;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 8;
        }
    },
    Fish {
        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public boolean canFly() {
            return false;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 1;
        }
    },
    Amphibians {
        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public boolean canFly() {
            return true;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 2;
        }
    },
    Reptiles {
        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public boolean canFly() {
            return true;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 4;
        }
    },
    Birds {
        @Override
        public boolean canSwim() {
            return false;
        }

        @Override
        public boolean canFly() {
            return true;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 2;
        }
    },
    Mammals {
        @Override
        public boolean canSwim() {
            return false;
        }

        @Override
        public boolean canFly() {
            return false;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 8;
        }
    }
}
