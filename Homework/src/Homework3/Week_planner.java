package Homework3;

import java.util.Scanner;

public class Week_planner {
    public static Scanner sc = new Scanner(System.in);
    public static String[][] scedule = new String[7][2];
    public static String input;

    public static void main(String[] args) {

        ///impementations
        scedule[0][0] = "sunday";
        scedule[1][0] = "monday";
        scedule[2][0] = "tuesday";
        scedule[3][0] = "wednesday";
        scedule[4][0] = "thursday";
        scedule[5][0] = "friday";
        scedule[6][0] = "saturday";
        ///taskss
        scedule[0][0] = "Do home works";
        scedule[0][1] = " Go to course";
        scedule[1][0] = "Meet with Freud";
        scedule[1][1] = "Drink";
        scedule[2][0] = "Begin project";
        scedule[3][0] = "Rest all day";
        scedule[4][0] = "Hack the Facebook";
        scedule[5][0] = "Invent Teleporting";
        scedule[6][0] = "Do nothing";
        //Mesagle
        System.out.println("Please, input the day of the week:");
        input = sc.next();
        while (true) {
            input = checkvalid(input);
            switch (input) {
                case "sunday":
                    printtask(0);
                    input = sc.next();
                    break;

                case "monday":
                    printtask(1);
                    input = sc.next();
                    break;

                case "tuesday":
                    printtask(2);
                    input = sc.next();
                    break;

                case "wednesday":
                    printtask(3);
                    input = sc.next();
                    break;

                case "thursday":
                    printtask(4);
                    input = sc.next();
                    break;

                case "friday":
                    printtask(5);
                    input = sc.next();
                    break;

                case "saturday":
                    printtask(6);
                    input = sc.next();
                    break;
            }
        }
    }

    static void printtask(int counter) {
        for (String s : scedule[counter])
            if (s != null) System.out.println(s);
    }

    static String checkvalid(String input) {
        while (!input.matches("^[a-zA-Z]*$")) {
            System.out.println("Sorry, I don't understand you, please try again");
            input = sc.next();
            break;
        }
        return input;
    }

}






