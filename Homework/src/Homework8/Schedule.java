package Homework8;

import java.util.ArrayList;
import java.util.HashMap;

public class Schedule {

    private HashMap<DayOfWeek, ArrayList<String>> schedule = new HashMap<DayOfWeek, ArrayList<String>>();

    {
        schedule.put(DayOfWeek.SUNDAY,new ArrayList<String>());
        schedule.put(DayOfWeek.SATURDAY,new ArrayList<String>());
        schedule.put(DayOfWeek.FRIDAY,new ArrayList<String>());
        schedule.put(DayOfWeek.THURSDAY,new ArrayList<String>());
        schedule.put(DayOfWeek.WEDNESDAY,new ArrayList<String>());
        schedule.put(DayOfWeek.TUESDAY,new ArrayList<String>());
        schedule.put(DayOfWeek.MONDAY,new ArrayList<String>());
    }


    public void NEwOne()
    {
        this.schedule = new HashMap<DayOfWeek, ArrayList<String>>()
        {{
            schedule.put(DayOfWeek.MONDAY,new ArrayList<String>());
            schedule.put(DayOfWeek.TUESDAY,new ArrayList<String>());
            schedule.put(DayOfWeek.WEDNESDAY,new ArrayList<String>());
            schedule.put(DayOfWeek.THURSDAY,new ArrayList<String>());
            schedule.put(DayOfWeek.FRIDAY,new ArrayList<String>());
            schedule.put(DayOfWeek.SATURDAY,new ArrayList<String>());
            schedule.put(DayOfWeek.SUNDAY,new ArrayList<String>());
        }};
    }

    public void add(DayOfWeek day, ArrayList<String> tasks)
    {
        this.schedule.get(day).addAll(tasks);
    }

    public void delete(DayOfWeek day, ArrayList<String> tasks)
    {
        this.schedule.get(day).removeAll(tasks);
    }
    @Override
    public String toString() {
        return "Schedule2{" +
                "schedule=" + schedule +
                '}';
    }

    public HashMap<DayOfWeek, ArrayList<String>> getSchedule() {
        return schedule;
    }
}
