package Homework8;

import java.util.*;

public class Family {
    private Human Mother;
    private Human Father;
    private List<Human> children;
    private Set<Pet> pet;

    protected Family(Human mother, Human father) {
        Mother = mother;
        Father = father;
        this.children = new ArrayList<>();
        this.pet = new HashSet<>();
    }


    protected void addChild(Human child) {
        children.add(child);
    }

    protected void deleteChild(Human child) {
        children.remove(child);
    }

    protected int countFamily() {
        return 2 + children.size();
    }


    private Human getMother() {
        return Mother;
    }

    private void setMother(Human mother) {
        Mother = mother;
    }

    private Human getFather() {
        return Father;
    }

    private void setFather(Human father) {
        Father = father;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }


    @Override
    public String toString() {
        return
                "Mother=" + Mother + "\n" +
                        " Father=" + Father + "\n" +
                        " children=" + children + "\n" +
                        " pet=" + pet +
                        '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(Mother, family.Mother) &&
                Objects.equals(Father, family.Father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Mother, Father, children, pet);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Family Removed");
    }

    public void AddPet(Set<Pet> reks) {
        pet.addAll(reks);
    }
}
