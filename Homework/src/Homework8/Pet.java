package Homework8;

import java.util.EnumSet;

public abstract class Pet {
    protected Species species;
    protected static String nickname;
    protected Integer age;
    protected Integer trickLevel;
    protected EnumSet<Habbits> habits;

    public Pet(Species species, String nickname, Integer age, Integer trickLevel, EnumSet<Habbits> habits) {
        this.species = Species.UNKNOWN;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    private void eat() {
        System.out.println("I am eating");
    }

    abstract void respond();

    private void foul() {
        System.out.println("I need to cover it up");
    }

    // setters and getters
    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public static String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(Integer trickLevel) {
        this.trickLevel = trickLevel;
    }

    public EnumSet<Habbits> getHabits() {
        return habits;
    }

    public void setHabits(EnumSet<Habbits> habits) {
        this.habits = habits;
    }

    ////
    protected void finalize() throws Throwable {
        System.out.println("Pet Removed");
    }

    @Override
    public String toString() {
        return "" +
                "nickname='" + nickname + '\'' +
                ", species=" + this.species.name().toLowerCase() +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits={" + habits.toString() +
                '}';
    }

}

