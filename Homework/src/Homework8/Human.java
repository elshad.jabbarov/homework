package Homework8;

import java.util.Objects;

public class Human implements Human_Interface {
    String Name;
    String Surname;
    Integer Year;
    Integer Iq;
    Schedule schedule = new Schedule();

    Human(String name, String surname, Integer year, Integer iq) {
        this.Name = name;
        this.Surname = surname;
        this.Year = year;
        this.Iq = iq;
    }

    public Human(String name, String surname, Integer year, Integer iq, Schedule schedule) {
        Name = name;
        Surname = surname;
        Year = year;
        Iq = iq;
        this.schedule.NEwOne();
    }

    public void greetPet() {
        System.out.printf("Hello, %s", Pet.getNickname());
    }


    private String getName() {
        return Name;
    }

    private void setName(String name) {
        this.Name = name;
    }

    private String getSurname() {
        return Surname;
    }

    private void setSurname(String surname) {
        this.Surname = surname;
    }

    private Integer getYear() {
        return Year;
    }

    private void setYear(Integer year) {
        this.Year = year;
    }

    private Integer getIq() {
        return Iq;
    }

    private void setIq(Integer iq) {
        this.Iq = iq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(Name, human.Name) &&
                Objects.equals(Surname, human.Surname) &&
                Objects.equals(Year, human.Year) &&
                Objects.equals(Iq, human.Iq);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Name, Surname, Year, Iq);
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + Name + '\'' +
                ", surname='" + Surname + '\'' +
                ", year=" + Year +
                ", iq=" + Iq +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Pet Removed");
    }

    @Override
    public void makeup() {

    }

    @Override
    public void repairCar() {

    }
}
