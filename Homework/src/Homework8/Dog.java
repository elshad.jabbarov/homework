package Homework8;

import java.util.EnumSet;
import java.util.Objects;

public class Dog extends Pet {
    Dog(Species species, String nickname, Integer age, Integer trickLevel, EnumSet<Habbits> habits) {
        super(species, nickname, age, trickLevel, habits);
        this.species = Species.MAMMALS;
        this.habits = habits;

    }

    @Override
    void respond() {
        System.out.printf("Hello, I am DOG, owner. I am %s. I miss you!", this.nickname);

    }

    public Species getSpecies() {
        return this.species;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;
        Pet myPet = (Pet) o;
        if (this.hashCode() != myPet.hashCode()) return false;
        return this.nickname.equals(myPet.getNickname()) && this.species.equals(myPet.getSpecies())
                && this.trickLevel.equals(myPet.getTrickLevel()) && this.age.equals(myPet.getAge()) &&
                this.species.equals(myPet.species);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, species, age, trickLevel, species);
    }
}
