package Homework8;

public interface Pet_Interface {
    boolean canFly();

    boolean canSwim();

    boolean hasFur();

    int numberOfLegs();
}
