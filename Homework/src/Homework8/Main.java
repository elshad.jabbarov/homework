package Homework8;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Pet BOXER = new Dog(Species.MAMMALS, "MEMO", 4, 2, EnumSet.of(Habbits.BARK, Habbits.PLAY));
        Pet Memo = new Fish(Species.FISH, "MEMO", 1, 10, EnumSet.of(Habbits.JUMP, Habbits.SLEEP));

        Set<Pet> Pets = new HashSet<>();
        Pets.add(BOXER);
        Pets.add(Memo);
        Human Father;
        Father = new Human("Albert", "Einstein", 1879, 15);
        Human Mother;
        Mother = new Human("Elsa", "Einstein", 1869, 16);
        Human Eduard;
        Eduard = new Human("Eduard", "Einstein", 1960, 100);
        Human Frank;
        Frank = new Human("Frank", "Einstein", 1970, 600);
        Family Einsteins = new Family(Father, Mother);
        Einsteins.addChild(Eduard);
        Einsteins.addChild(Frank);
        Einsteins.AddPet(Pets);
        System.out.println(Einsteins.toString());

        System.out.println(" The Number of Family member is  " + Einsteins.countFamily());

    }
}
