package Homework8;

public class Man extends Human {
    public Man(String name, String surname, Integer year, Integer iq) {
        super(name, surname, year, iq);
    }


    @Override
    public void repairCar() {
        System.out.println("I am a man I can fix the car");
    }

}
