package Homework7;

public class Woman extends Human {
    public Woman(String name, String surname, Integer year, Integer iq) {
        super(name, surname, year, iq);
    }

    @Override
    public void makeup() {
        System.out.println(" I am a woman I love makeup");
    }
}
