package Homework7;

public interface Interface {
    boolean canFly();

    boolean canSwim();

    boolean hasFur();

    int numberOfLegs();
}
