package Homework7;

import java.util.Objects;

public class DomesticCat extends Pet {
    public DomesticCat(Species species, String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(species, nickname, age, trickLevel, habits);
        this.species = Species.MAMMALS;
    }

    @Override
    void respond() {
        System.out.printf("Hello, owner. I am %s. I miss you!", this.nickname);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;
        Pet myPet = (Pet) o;
        if (this.hashCode() != myPet.hashCode()) return false;
        return this.nickname.equals(myPet.getNickname()) && this.species.equals(myPet.getSpecies())
                && this.trickLevel.equals(myPet.getTrickLevel()) && this.age.equals(myPet.getAge()) &&
                this.species.equals(myPet.species);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, species, age, trickLevel, species);
    }


}
