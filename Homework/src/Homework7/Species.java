package Homework7;

import org.omg.CORBA.UNKNOWN;

public enum Species implements Interface {
    INVERTEBRATES {
        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public boolean canFly() {
            return true;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 8;
        }
    },
    FISH {
        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public boolean canFly() {
            return false;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 1;
        }
    },
    AMPHIBIANS {
        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public boolean canFly() {
            return true;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 2;
        }
    },
    REPTILES {
        @Override
        public boolean canSwim() {
            return true;
        }

        @Override
        public boolean canFly() {
            return true;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 4;
        }
    },
    BIRDS {
        @Override
        public boolean canSwim() {
            return false;
        }

        @Override
        public boolean canFly() {
            return true;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 2;
        }
    },
    MAMMALS {
        @Override
        public boolean canSwim() {
            return false;
        }

        @Override
        public boolean canFly() {
            return false;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 8;
        }
    },
    ROBOCAT {
        @Override
        public boolean canSwim() {
            return false;
        }

        @Override
        public boolean canFly() {
            return false;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 4;
        }
    },
    UNKNOWN {
        @Override
        public boolean canSwim() {
            return false;
        }

        @Override
        public boolean canFly() {
            return false;
        }

        @Override
        public boolean hasFur() {
            return true;
        }

        @Override
        public int numberOfLegs() {
            return 4;
        }

    }


}
