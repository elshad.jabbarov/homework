package Homework4;

public class Main {
    public static void main(String[] args) {

        Human Father;
        Father = new Human("Albert", "Einstein", 1879);
        Human Mother;
        Mother = new Human("Elsa", "Einstein", 1869);
        Pet dog = new Pet("dog", "rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human Child;
        Child = new Human("Eduard", "Einstein", 1960, 100, dog, Father, Mother);
        System.out.println(Child.toString());
        System.out.println("Mother of child is  " + Child.mother.getName());
        System.out.println("Father of child is  " + Child.father.getName());
        System.out.println("Child's name is  " + Child.getName());
        System.out.println(" Child's pet is  " + Child.pet);


    }
}
