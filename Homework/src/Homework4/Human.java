package Homework4;

import java.util.Arrays;

public class Human {
    String name;
    String surname;
    Integer year;
    Integer iq;
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule;

    public Human(String name, String surname, Integer year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human() {
    }

    public Human(String name) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Integer getYear() {
        return year;
    }

    public Integer getIq() {
        return iq;
    }

    public Pet getPet() {
        return pet;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public Human(String name, String surname, Integer year, Integer iq, Pet pet, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;

    }

    void greetPet() {
        System.out.println("Hello, " + pet.getNickname());
    }

    void describePet() {
        String trik;
        if (pet.getTrickLevel() > 50) trik = "very sly";
        else trik = "almost not sly";
        System.out.println("I have a " + pet.getSpecies() + " he is " + pet.getAge() + " years old" + " he is " + trik);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", schedule=" + Arrays.toString(schedule) +
                '}';
    }
}
